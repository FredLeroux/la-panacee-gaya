package com.gaya.toolsService;

import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author Frédéric Leroux <br>
 *         Contains tools for API LaPanaceeGaya
 */
public interface ToolsService {
    /**
     *
     * @param pHashMap a hashMap set with key as String and Value as String
     * @return a TreeMap containing the HashMap indexed, natural ordered with for
     *         each index corresponding keys and values
     */
    public TreeMap<String, TreeMap<String, String>> indexedList(HashMap<String, String> pHashMap);
}
