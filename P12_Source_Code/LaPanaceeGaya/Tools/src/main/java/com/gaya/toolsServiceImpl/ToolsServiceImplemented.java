package com.gaya.toolsServiceImpl;

import java.util.HashMap;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.gaya.toolsService.ToolsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ToolsServiceImplemented implements ToolsService {

    private static final String K = "call";
    private static final String I = "in";
    private static final String O = "out";

    @Override
    public TreeMap<String, TreeMap<String, String>> indexedList(HashMap<String, String> pHashMap) {
	TreeMap<String, TreeMap<String, String>> vIndexedList = new TreeMap<String, TreeMap<String, String>>();
	pHashMap.forEach((k, v) -> addStringToIndex(vIndexedList, k, v));
	return vIndexedList;
    }

    /**
     *
     * @param pStr the String to test
     * @return the first character of pStr as a String
     */
    private String firstLetter(String pStr) {
	log.debug(K);
	return pStr.toUpperCase().trim().substring(0, 1);
    }

    /**
     *
     * @param pIndex the TreeMap to fill
     * @param pValue the String to add to the pIndex if pIndex.get(the first
     *               character of pStr) not null add pStr to the ArrayList else
     *               put(the first character of pStr, new ArrayList.add pStr)
     */
    private void addStringToIndex(TreeMap<String, TreeMap<String, String>> pIndex, String pKey, String pValue) {
	log.debug(I);
	String vStr = firstLetter(pValue);
	if (pIndex.get(vStr) != null) {
	    log.debug("add");
	    pIndex.get(vStr).put(pKey, pValue);
	} else {
	    log.debug("create and add");
	    TreeMap<String, String> vTreeMap = new TreeMap<String, String>();
	    vTreeMap.put(pKey, pValue);
	    pIndex.put(vStr, vTreeMap);
	}
	log.debug(O);
    }

}
