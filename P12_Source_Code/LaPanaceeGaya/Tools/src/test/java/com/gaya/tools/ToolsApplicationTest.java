package com.gaya.tools;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.TreeMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.gaya.toolsServiceImpl.ToolsServiceImplemented;

@SpringBootTest(classes = { ToolsServiceImplemented.class })
public class ToolsApplicationTest {

    private HashMap<String, String> testHashMap;
    private ToolsServiceImplemented tools = new ToolsServiceImplemented();

    @BeforeEach
    public void init() {
	testHashMap = new HashMap<String, String>();
	testHashMap.put("10", "ZzzZ");
	testHashMap.put("2", "Baa");
	testHashMap.put("3", "Aaa");
	testHashMap.put("8", "Aab");
	testHashMap.put("4", "bba");
	testHashMap.put("1", "Zaa");

    }

    @Test
    public void indexedListTest() {
	TreeMap<String, TreeMap<String, String>> tMap = tools.indexedList(testHashMap);
	assertThat(tMap).isNotNull();
	assertThat(tMap.size()).isEqualTo(3);
	assertThat(tMap.get("A").size()).isEqualTo(2);
	assertThat(tMap.get("Z").get("10")).isEqualTo("ZzzZ");

    }

    @Test
    public void indexedListTestEmptyList() {
	testHashMap.clear();
	TreeMap<String, TreeMap<String, String>> tMap = tools.indexedList(testHashMap);
	assertThat(tMap).isEmpty();
    }

}
