# **OPENCLASSROOMS**
## PARCOURS : Développeur d'application - Java
## _PROJET 12 : Aidez la communauté en tant que développeur d'application Java_
### _Etudiant : Frédéric Leroux_
# **STATUT DU PROJET**
### _Version        :   **Alpha**_
### _Développement  :   **En cours**_
### _Production     :   **Néant**_
# **ETAT DU PROJET**
[![pipeline status](https://gitlab.com/FredLeroux/la-panacee-gaya/badges/api/pipeline.svg)](https://gitlab.com/FredLeroux/la-panacee-gaya/-/commits/master)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=coverage)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=bugs)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=code_smells)](https://sonarcloud.io/dashboard?id=laPanacee)\
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=ncloc)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=alert_status)](https://sonarcloud.io/dashboard?id=laPanacee)\
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=security_rating)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=sqale_index)](https://sonarcloud.io/dashboard?id=laPanacee)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=laPanacee)\
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=laPanacee&metric=alert_status)](https://sonarcloud.io/dashboard?id=laPanacee)

# **DETAILS DU PROJET**
## Nom : 
#### _La panacée Gaya_
## Qu'est ce que c'est :
#### _Le projet la panacée Gaya est une adaptation numérique du livre "Mes 15 huiles essentielles" de [Danièle Festy](https://www.danielefesty.com/), dont le but est de faciliter la recherche et l'accès aux connaissances contenues dans ce livre._
## Pourquoi :
#### _Cette API en plus d'apporter une certaine praticité et facilité d'utilisation, à aussi pour but de démontrer l'ensemble des connaissances et compétences acquises lors du parcours openclassrooms développeur d'application java._
#### _**Ce projet est composé d'une base de donné MySQL, d'un Back-End JDK 11  RestFull utilisant Spring Boot, d'un Front-End Optionel Angular (non couvert lors du parcours, mais une utilisation sommaire pour ce projet en guise de premiere approche), l'ensemble des fonctionalités seront démontrées via postman et des appels depuis un navigateur**_
## Avertissement :
#### ___Attention bien que les huiles essentielles aient démontré une certaine efficacité, les méthodes de soins proposées dans cette API (qui sont surtout des traitement de fond), ne se substituent en aucun cas à une consultation ou avis médical d'un professionnel de santé.___

## Environement :
![maven central](https://img.shields.io/maven-central/v/org.apache.maven/apache-maven?color=informational&style=plastic)\
[![spring-boot - v2.4.6](https://img.shields.io/badge/spring--boot-v2.4.6-informational?style=plastic)](https://https://spring.io/)\
[![jdk - v11.0.10](https://img.shields.io/badge/jdk-v11.0.10-informational?style=plastic)](https://www.oracle.com/index.html)\
[![angular - 11](https://img.shields.io/badge/angular-v12.0.3-informational?style=plastic)](https://angular.io/)\
[![model-mapper - v2.4.3](https://img.shields.io/badge/model--mapper-v2.4.3-informational?style=plastic)](http://modelmapper.org/)\
[![MySQL - v8](https://img.shields.io/badge/MySQL-v8-informational?style=plastic)](https://www.mysql.com/)
## Git branch de développement actuel
### _branche la plus avancée actuellement_
```sh
cd path to project folder/
git checkout api
``` 
## MySQL
#### _Les informations de la base de données sont disponible dans le dossier :  Git Branch api // path to project folder/P12_BDD_


## Maven
#### Install
```sh
cd path to LaPanaceeGaya module folder/
mvn clean install
```
#### Tests
```sh
cd path to LaPanaceeGaya module folder/
mvn test -PuniTests
mvn test -PintegrationTests
mvn test -PfullTests
```
#### Site 
```sh
cd path to LaPanaceeGaya module folder/
clean verify site site:deploy -PfullTests
```
## Spring Boot 
#### Run (localhost:7000/)
```sh
cd path to la-panacee-gaya-app module folder/
mvn clean install spring-boot:run -Dspring-boot.run.profiles
```
#### Test (localhost:9100/)
#### _Une copie de la basse de donnée est réalisée avec une base H2_
```sh
cd path to la-panacee-gaya-app module folder/
mvn clean install spring-boot:run -Dspring-boot.run.profiles=test
```
## Angular
#### _le code source du Front est disponible dans le dossier Git Branch api // path to project folder/P12_Front_
```sh
cd path to project folder/P12_Front/LaPanaceeGayaFront/dist/LaPanaceeGayaFront
ng serve --open
"keep in mind to run the api on test see Spring Boot test "
```
## Postman
#### _Une collection postman est disponible dans le dossier : Git Branch api // path to project folder/P12_Postman_Request_
##  Progression estimée
#### _Git Branch api_
Base de données     [/////////////////////////////////////////////////////// ] 100 % \
Back-End            [////////////////////////////////////////_______________ ] 70 % \
Front-End           [////___________________________________________________ ] 5% \
Avancement total    [////////////////////////////////_______________________ ] 60%
## Statut
#### _Projet en version Alpha ce qui implique que les fonctionalités actuellemnt implémenté sont en cours de développemnt et en cours de POC (proof of concept)_
#### Liste des fonctionalités implémentées :
| Fonctionalités | DataBase | BACK-END | FRONT-END
| ------ | ------ | ------ | ------ |
| Sécurité via spring security et jwt | Ok | Ok | KO |
| CRUD Huiles | Ok | Ok | KO |
| CRUD Troubles | Ok | Ok | KO |
| CRUD Recettes| Ok | Ok | KO |
| CRUD Utilisisateurs| Ok | Ok | KO |
| Index huiles | Ok | Ok | KO |
| Index troubles | Ok | KO | KO |
| Recettes en fonction d'une huile | Ok | OK | KO |
| Recettes en fonction d'un trouble | Ok | KO | KO |
| Controller | N/A | ON-GOING | KO |
| Gestion d'un compte utilisateur | Next-Version |  Next-Version |  Next-Version |
| Mailling | Next-Version |  Next-Version |  Next-Version |
| Auto BackUp DataBase | Next-Version |  Next-Version |  Next-Version |
| ------ | ------ | ------ | ------ |
## Informations complémentaires :

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/dashboard?id=laPanacee)
## Remerciments :
#### **Je tiens à remercier les personnes, communautés et organismes suivant pour leur partage de connaissances et leur travail sur les tutoriels qu'ils proposent.**
[![à - Baeldung](https://img.shields.io/badge/à-Baeldung-blueviolet)](https://www.baeldung.com/)\
[![à - bezkoder](https://img.shields.io/badge/à-bezkoder-blueviolet)](https://bezkoder.com/)\
[![à - blog.paumard](https://img.shields.io/badge/à-blog.paumard-blueviolet)](http://blog.paumard.org/)\
[![à - dillinger.io](https://img.shields.io/badge/à-dillinger.io-blueviolet)](https://dillinger.io/)\
[![à - GitHub](https://img.shields.io/badge/à-GitHub-blueviolet)](https://github.com/)\
[![à - howtodoinjava](https://img.shields.io/badge/à-howtodoinjava-blueviolet)](https://howtodoinjava.com/)\
[![à - mkyong](https://img.shields.io/badge/à-mkyong-blueviolet)](https://mkyong.com/)\
[![à - stackoverflow](https://img.shields.io/badge/à-stackoverflow-blueviolet)](https://stackoverflow.com/)\
[![à - w3schools](https://img.shields.io/badge/à-w3schools-blueviolet)](https://www.w3schools.com/)\
\




